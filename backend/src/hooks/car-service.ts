// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
//I ended up creating the wrong hook name and I think that threw everything out of proportion as node wasn't starting for a while because it could not find that file
// I thought i changed the configuration to no longer look for it but this was not working
// alert should be replaced with syntax for throwing errors
export default (options = {}): Hook => {
    
  return async (context: HookContext) => {
    const { result } = context;
    console.log(...result); // returning undefined! 
    if((result['make'].length + result['model'].length) > 30){
        alert("make and model cannot be more than 30 characters");
        return context;
    }else if(result['make'].length === 0  || result['model'].length === 0){
        alert("make and model must be defined");
        return context;
    }else if(result['year'] < 1885 || result['year'] > 2020){
        alert("Must enter a valid year");
        return context;
    }else if(result['mileage'] < 1){
        alert("Enter your mileage");
        return context;
    }
    return context;
  };
  
}
